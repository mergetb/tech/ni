#!/bin/bash

ARGS="--config-file mypy.ini"

mypy $ARGS ni.py
mypy $ARGS server.py
